package com.firebasechatapp.firebaseauth.model;

import io.realm.RealmObject;

public class UserLogin extends RealmObject {

    //@PrimaryKey
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Override
    public String toString() {
        return name;
    }


}

package com.firebasechatapp.firebaseauth.model;

public class ChatDetails {
    private String name;
    private String message;
    private String time;
    private String date;
    private String typeMessage;
    private int type;

    public ChatDetails(String name, String message, String time, String date, String typeMessage, int type) {
        this.name = name;
        this.message = message;
        this.time = time;
        this.date = date;
        this.typeMessage = typeMessage;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTypeMessage() {
        return typeMessage;
    }

    public void setTypeMessage(String typeMessage) {
        this.typeMessage = typeMessage;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
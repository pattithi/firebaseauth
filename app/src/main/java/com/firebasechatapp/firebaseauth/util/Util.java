package com.firebasechatapp.firebaseauth.util;

import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

public class Util {

    public static final String URL_STORAGE_REFERENCE = "gs://testfirebase-d6638.appspot.com";
    public static final String FOLDER_STORAGE_IMG = "images";
    public static final String FOLDER_STORAGE_PROFILE_IMG = "images/profile";

    public static void showLog(String message){
        Log.d("LOG UTILS TEST", message);
    }
    public static void setSnackBar(View view, String message){
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

}

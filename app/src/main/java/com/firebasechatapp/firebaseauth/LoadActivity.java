package com.firebasechatapp.firebaseauth;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import com.firebase.client.Firebase;
import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.firebasechatapp.firebaseauth.model.UserLogin;
import com.firebasechatapp.firebaseauth.module.login.LoginFragment;
import com.firebasechatapp.firebaseauth.module.room.RoomFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class LoadActivity extends AppCompatActivity {

    @BindView(R.id.fl_container)
    FrameLayout flContainer;

    // Google configure
    public FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;

    //Realm
    Realm realm;
    ////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);
        ButterKnife.bind(this);

        Realm.init(getApplicationContext());
        realm = Realm.getDefaultInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() == null) {
                    /*startActivity(new Intent(HomeActivity.this, LoadActivity.class));
                    finish();*/
                }
            }
        };

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            UserDetails.username = user.getDisplayName();
            UserDetails.email = user.getEmail();

            nextToRoomFragment();
            Log.d("LoadActivity", "Found google user: " + user.getDisplayName());
        } else if (!realm.isEmpty()) {
            Firebase.setAndroidContext(this);
            RealmResults<UserLogin> persons = realm.where(UserLogin.class).findAll();
            final String name = persons.get(0).toString();
            UserDetails.username = name;

            nextToRoomFragment();
            Log.d("Found realm user ", name);
        } else {
            nextToLoginFragment();
            Log.d("LoadActivity", "Not found user");
        }
    }

    public void nextToRoomFragment(){
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.add(flContainer.getId(), RoomFragment.newInstance());
        t.commit();

        /*startActivity(new Intent(LoadActivity.this, HomeActivity.class));
        finish();*/

    }
    public void nextToLoginFragment(){
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.add(flContainer.getId(), LoginFragment.newInstance());
        t.commit();
    }

}


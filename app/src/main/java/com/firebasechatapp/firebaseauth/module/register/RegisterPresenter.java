package com.firebasechatapp.firebaseauth.module.register;


import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;

import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RegisterPresenter implements RegisterConstructor.RegisterSetPresenter {

    private RegisterConstructor.RegisterView registerView;
    private DatabaseReference database, usersChildDatabase, usersChildren;

    public RegisterPresenter(RegisterConstructor.RegisterView registerView) {
        this.registerView = registerView;
    }

    public void userRegister(final String username, final String password, final String deviceToken, final String currentDeviceId) {

        database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
        usersChildDatabase = database.child("User");
        usersChildren = usersChildDatabase.child(username);

        usersChildDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(username)) {
                    usersChildren.child("Username").setValue(username);
                    usersChildren.child("Password").setValue(password);
                    usersChildren.child("DeviceId").setValue(currentDeviceId);
                    usersChildren.child("DeviceToken").setValue(deviceToken);

                    registerView.onRegisterSuccess(username, password);
                    //registerView.showToast("Register success");
                } else {
                    registerView.onTextRegisterFound();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("TAG", "onCancelled", databaseError.toException());
            }
        });
    }
    
    @Override
    public void clickLogin() {
        registerView.onClickToLogin();
    }

}

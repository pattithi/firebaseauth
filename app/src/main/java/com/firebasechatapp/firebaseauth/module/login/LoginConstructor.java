package com.firebasechatapp.firebaseauth.module.login;

import android.view.View;

public class LoginConstructor {
    public interface LoginSetPresenter {
        void userLogin(String username, String password);
        void googleRegister(String name, String email, String deviceToken, String currentDeviceId);
        void clickRegister();
        void setSnackBar(View view);
    }

    public interface LoginView {
        void onLoginSuccess();
        void onClickToRegister();

        void onShowToast(String message);
        void onLoadShow();
        void onLoadDismiss();
        void onTextLoginNotFound();
        void onTextLoginPassIncorrect();
    }

}

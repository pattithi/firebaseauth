package com.firebasechatapp.firebaseauth.module.chat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.firebasechatapp.firebaseauth.R;
import com.firebasechatapp.firebaseauth.model.ChatDetails;
import com.firebasechatapp.firebaseauth.module.chat.friend.FriendFragment;

import org.parceler.Parcels;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;


public class ChatFragment extends Fragment implements ChatConstructor.ChatView, View.OnClickListener, View.OnFocusChangeListener, View.OnTouchListener {

    @BindView(R.id.recycler_chat)
    RecyclerView recyclerView;
    Unbinder unbinder;

    @BindView(R.id.tv_room_name)
    TextView tvRoomName;
    @BindView(R.id.tv_chat_head)
    LinearLayout tvRoomHead;
    /*@BindView(R.id.rv_sub_menu)
    RelativeLayout rvSubMenu;
    @BindView(R.id.rv_sub_sub_menu)
    RelativeLayout rvSubSubMenu;*/
    @BindView(R.id.edt_message)
    EditText edtChatMessage;
    @BindView(R.id.btn_send_message)
    ImageView btnChatSentMessage;
    @BindView(R.id.btn_more_add)
    ImageView btnMoreAdd;
    @BindView(R.id.btn_camera_add)
    ImageView btnCameraAdd;
    @BindView(R.id.iv_chat_all_friend)
    ImageView ivAllFriend;
    @BindView(R.id.iv_chat_setting)
    ImageView ivSetting;

    private ChatAdapter adapter;
    private ChatConstructor.ChatSetPresenter chatPresenters;
    private String roomName;
    private Dialog dialog;
    private LinearLayoutManager layoutManager;

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final String TYPE_MESSAGE_TEXT = "text";
    /*private File filePathImageCamera;
    File file;
    Uri fileUri;*/

    private ProgressDialog loadingDialog;
    private static final String TAG = ChatFragment.class.getSimpleName();
    //////////////////////////////////////

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        roomName = Parcels.unwrap(getArguments().getParcelable("key"));

        /*layoutManager = new LinearLayoutManager(getContext());
        chatPresenters = new ChatPresenters(this, getContext(), roomName);
        chatPresenters.getMessages(roomName);
        adapter = new ChatAdapter();*/

    }

    @Override
    public void onResume() {
        super.onResume();

        layoutManager = new LinearLayoutManager(getContext());
        chatPresenters = new ChatPresenters(this, getContext(), roomName);
        chatPresenters.getMessages(roomName);
        adapter = new ChatAdapter();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.chat_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvRoomName.setText(roomName);
        btnChatSentMessage.setVisibility(View.GONE);
        /*rvSubMenu.setVisibility(View.GONE);
        rvSubSubMenu.setVisibility(View.GONE);*/

        recyclerView.setOnFocusChangeListener(this);
        //rvSubMenu.setOnTouchListener(this);
        edtChatMessage.setOnFocusChangeListener(this);
        btnChatSentMessage.setOnClickListener(this);
        btnMoreAdd.setOnClickListener(this);
        btnCameraAdd.setOnClickListener(this);
        ivAllFriend.setOnClickListener(this);
        ivSetting.setOnClickListener(this);

    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getMessagesSuccess(ArrayList<ChatDetails> chatArrayList) {

        adapter.setChatAdapter(getContext(), chatArrayList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onHideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onClickToFriendList() {

        FriendFragment friendFragment = new FriendFragment();

        Bundle bundle = new Bundle();
        Parcelable wrapped = Parcels.wrap(roomName);
        bundle.putParcelable("key", wrapped);
        friendFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, friendFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    public void onSettingDialog(final String roomName) {

        String nList[] = {"Password", "Picture"};

        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.room_setting_dialog, null);
        final ListView listview = view.findViewById(R.id.lv_setting);

        listview.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, nList));
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                showToast("Click" + " " + String.valueOf(position));
                if (position == 0) {
                    dialog.dismiss();
                    changePassDialog(roomName);
                } else if (position == 1) {
                    dialog.dismiss();
                    changePicDialog(roomName);
                }
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public void onImageLoadDialog() {
        loadingDialog = new ProgressDialog(getContext());
        loadingDialog.setMessage("Loading...");
        loadingDialog.show();
    }

    @Override
    public void onImageDismissDialog() {
        loadingDialog.dismiss();
    }

    private void changePicDialog(final String roomName) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.room_setting_pic_dialog, null);
        final EditText edtRoomPic = view.findViewById(R.id.edt_pic_edt);
        final TextView btnConfirm = view.findViewById(R.id.tv_click_confirm);
        final TextView btnCancel = view.findViewById(R.id.tv_click_cancel);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPicRoom = edtRoomPic.getText().toString().trim();
                chatPresenters.editNewPic(roomName, newPicRoom);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    private void changePassDialog(final String roomName) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.room_setting_pass_dialog, null);
        final EditText edtRoomPass = view.findViewById(R.id.edt_pass_edt);
        final TextView btnConfirm = view.findViewById(R.id.tv_click_confirm);
        final TextView btnCancel = view.findViewById(R.id.tv_click_cancel);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newPassRoom = edtRoomPass.getText().toString().trim();
                chatPresenters.editNewName(roomName, newPassRoom);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    private void photoCameraIntent() {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 0);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, IMAGE_CAMERA_REQUEST);
            Log.d(TAG, "photoCameraIntent: " + intent);
        }
    }

    private void photoGalleryIntent() {

        //intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        //startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);
        //Log.e("Request", String.valueOf(intent));

        try {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, IMAGE_GALLERY_REQUEST);
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);

                /*Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                Log.d(TAG, "photoGalleryIntent: " + galleryIntent);*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permission GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                //Log.d(TAG, "Gallery data: " + data + " " + resultCode + " " + RESULT_OK);
                chatPresenters.sendPhotoGalleryMessage(requestCode, IMAGE_GALLERY_REQUEST, data);
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] bytes = baos.toByteArray();

                chatPresenters.sendPhotoCameraMessage(requestCode, IMAGE_CAMERA_REQUEST, bytes);
                //Log.d(TAG, "Camera data: " + bytes + " " + resultCode + " " + RESULT_OK);
            }
        }
    }


    @Override
    public void dismissSubMenu() {
        Animation animSlideUp = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
        /*rvSubSubMenu.startAnimation(animSlideUp);
        rvSubSubMenu.setVisibility(View.GONE);

        Animation animFadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
        rvSubMenu.startAnimation(animFadeOut);
        rvSubMenu.setVisibility(View.GONE);*/

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send_message:
                String message = edtChatMessage.getText().toString();
                if (!message.equals("")) {
                    chatPresenters.sendMessage(message, roomName, TYPE_MESSAGE_TEXT);
                    edtChatMessage.setText("");
                }
                break;
            case R.id.btn_more_add:
                photoGalleryIntent();
                break;
            case R.id.btn_camera_add:
                photoCameraIntent();
                break;
            case R.id.iv_chat_all_friend:
                chatPresenters.clickShowFriend();
                break;
            case R.id.iv_chat_setting:
                chatPresenters.clickSetting();
                /*if (rvSubSubMenu.getVisibility() == View.VISIBLE) {
                    Animation animSlideUp = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
                    rvSubSubMenu.startAnimation(animSlideUp);
                    rvSubSubMenu.setVisibility(View.GONE);
                } else {
                    rvSubSubMenu.setVisibility(View.VISIBLE);
                    Animation animSlideDown = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down);
                    rvSubSubMenu.startAnimation(animSlideDown);
                }

                if (rvSubMenu.getVisibility() == View.VISIBLE) {
                    Animation animFadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                    rvSubMenu.startAnimation(animFadeOut);
                    rvSubMenu.setVisibility(View.GONE);
                } else {
                    rvSubMenu.setVisibility(View.VISIBLE);
                    Animation animFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
                    rvSubMenu.startAnimation(animFadeIn);
                }*/
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.recycler_chat:
                if (hasFocus) {
                    onHideKeyboard(view);
                }
                break;
            case R.id.edt_message:
                btnChatSentMessage.setVisibility(View.VISIBLE);
                if (hasFocus) {
                    //dismissSubMenu();
                }
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (view.getId()) {
            /*case R.id.rv_sub_menu:
                if (rvSubMenu.getVisibility() == View.VISIBLE) {
                    rvSubMenu.setVisibility(View.GONE);
                }
                break;
            case R.id.rv_sub_sub_menu:
                if (rvSubSubMenu.getVisibility() == View.VISIBLE) {
                    rvSubSubMenu.setVisibility(View.GONE);
                    rvSubMenu.setVisibility(View.GONE);
                }
                break;*/
        }
        return false;
    }
    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_setting, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_calls:
                Toast.makeText(getActivity(), "Calls Icon Click", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }*/
}

package com.firebasechatapp.firebaseauth.module.room;

public interface RoomRealmData {
    void removeCurrent();
}

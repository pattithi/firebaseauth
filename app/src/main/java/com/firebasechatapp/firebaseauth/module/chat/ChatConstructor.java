package com.firebasechatapp.firebaseauth.module.chat;

import android.content.Intent;
import android.view.View;

import com.firebasechatapp.firebaseauth.model.ChatDetails;

import java.util.ArrayList;


public class ChatConstructor {

    public interface ChatSetPresenter {
        void getMessages(String roomName);
        void sendMessage(String message, String roomName, String type_message);
        void clickShowFriend();
        void clickSetting();
        void editNewName(String roomName, String newPassRoom);
        void editNewPic(String roomName, String newPicRoom);
        void sendPhotoGalleryMessage(int requestCode, int IMAGE_GALLERY_REQUEST, Intent data);
        void sendPhotoCameraMessage(int requestCode, int IMAGE_GALLERY_REQUEST, byte[] data);
    }

    public interface ChatView {
        void showToast(String message);
        void getMessagesSuccess(ArrayList<ChatDetails> chatArrayList);
        void onHideKeyboard(View view);
        void onClickToFriendList();
        void onSettingDialog(String roomName);
        void onImageLoadDialog();
        void onImageDismissDialog();
        void dismissSubMenu();
    }
}

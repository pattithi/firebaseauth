package com.firebasechatapp.firebaseauth.module.chat.friend;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.firebasechatapp.firebaseauth.R;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FriendFragment extends Fragment implements FriendConstructor.FriendView, View.OnClickListener {

    @BindView(R.id.recycler_friend)
    RecyclerView recyclerView;
    Unbinder unbinder;

    @BindView(R.id.tv_friend_empty)
    TextView tvFriendEmpty;
    @BindView(R.id.iv_chat_add_friend)
    ImageView ivAddFriend;

    private FriendConstructor.FriendSetPresenter friendPresenters;
    private FriendAdapter adapter;
    private LinearLayoutManager layoutManager;
    private String roomName;
    private Dialog dialog;

    private static final String TAG = FriendFragment.class.getSimpleName();
    //////////////////////////////////////

    /*public static ChatFragment newInstance() {
        return new ChatFragment();
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        roomName = Parcels.unwrap(getArguments().getParcelable("key"));

    }

    @Override
    public void onResume() {
        super.onResume();

        friendPresenters = new FriendPresenter(this);
        friendPresenters.getFriend(roomName);
        adapter = new FriendAdapter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.friend_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);

        ivAddFriend.setOnClickListener(this);

        return v;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void getFriendSuccess(ArrayList<String> friendList, int totalFriend) {

        /*if (totalFriend < 1) {
            tvFriendEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            tvFriendEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.setFriendAdapter(getContext(),friendList, this);
        }

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);*/

        Log.e("TAGTAGTAGG", String.valueOf(adapter + " " + layoutManager + " " + recyclerView));
    }

    @Override
    public void onAddFriendDialog(ArrayList<String> nameList, int size) {

        final ArrayList<String> nList = nameList;
        final ArrayList nameSelected = new ArrayList<>();

        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.room_add_friend_dialog, null);
        final TextView btnConfirm = view.findViewById(R.id.tv_click_confirm);
        final TextView btnCancel = view.findViewById(R.id.tv_click_cancel);
        final ListView listview = view.findViewById(R.id.lv_nameSelected);

        if (!nList.equals(null)) {
            listview.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_list_item_multiple_choice, nList));
            listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    if (listview.isItemChecked(position)) {
                        nameSelected.add(adapterView.getItemAtPosition(position));
                    } else if (nameSelected.contains(adapterView.getItemAtPosition(position))) {
                        nameSelected.remove(adapterView.getItemAtPosition(position));
                    }
                }
            });
        }

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                friendPresenters.setAddRoomFriend(nameSelected, roomName);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public void onRemoveDialog(final String friendName) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.friend_remove_dialog, null);

        final TextView btnRemove = view.findViewById(R.id.tv_click_remove);
        final TextView btnCancel = view.findViewById(R.id.tv_click_cancel);

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                friendPresenters.removeFriend(friendName, roomName);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_chat_add_friend:
                friendPresenters.clickShowFriend();
                showToast("Show Friend");
                break;
        }
    }
}

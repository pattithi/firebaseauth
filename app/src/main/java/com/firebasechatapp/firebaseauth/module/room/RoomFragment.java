package com.firebasechatapp.firebaseauth.module.room;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebasechatapp.firebaseauth.R;
import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.firebasechatapp.firebaseauth.module.chat.ChatFragment;
import com.firebasechatapp.firebaseauth.module.chats.ChatAllFragment;
import com.firebasechatapp.firebaseauth.module.login.LoginFragment;
import com.google.firebase.auth.FirebaseAuth;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.view.View.GONE;


public class RoomFragment extends Fragment implements RoomConstructor.RoomView, View.OnClickListener {

    @BindView(R.id.recycler_room)
    RecyclerView recyclerView;
    Unbinder unbinder;

    @BindView(R.id.ln_public_room)
    LinearLayout publicRoomClick;
    @BindView(R.id.tv_me)
    TextView tvMe;
    @BindView(R.id.tv_room_empty)
    TextView tvRoomEmpty;
    @BindView(R.id.tv_room_logout)
    TextView ivRoomLogout;
    @BindView(R.id.iv_room_add)
    ImageView ivRoomAdd;
    @BindView(R.id.iv_room_setting)
    ImageView ivSetting;
    @BindView(R.id.progress_room_main)
    ProgressBar progressRoomLoad;

    private RoomConstructor.RoomSetPresenter roomPresenters;
    private RoomAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Dialog dialog;
    private FirebaseAuth mAuth;

    private static final String TAG = RoomFragment.class.getSimpleName();
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static RoomFragment newInstance() {
        return new RoomFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layoutManager = new LinearLayoutManager(getContext());
        roomPresenters = new RoomPresenter(this);
        //roomPresenters.getRoom();
        adapter = new RoomAdapter();


    }

    @Override
    public void onResume() {
        super.onResume();

        /*layoutManager = new LinearLayoutManager(getContext());
        roomPresenters = new RoomPresenter(theis);*/
        roomPresenters.getRoom();
        //adapter = new RoomAdapter();
        //homeActivity.navigationShow();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.room_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);

        hideSoftKeyboard(v);

        /*FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            String name = user.getDisplayName();
            String email = user.getEmail();
            String uid = user.getUid();
        }*/

        //tvEmail.setText(UserDetails.email);
        //tvName.setText(UserDetails.username);

        /*logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserDetails.username = "";
                UserDetails.email = "";

                mAuth.signOut();

                realm = Realm.getDefaultInstance();
                final RealmResults<UserLogin> persons = realm.where(UserLogin.class).findAll();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        persons.deleteAllFromRealm();
                    }
                });

                LoginFragment loginFragment = new LoginFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fl_container, loginFragment);
                fragmentTransaction.commit();
            }
        });*/

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        tvMe.setText(UserDetails.username);

        ivRoomAdd.setOnClickListener(this);
        ivSetting.setOnClickListener(this);
        ivRoomLogout.setOnClickListener(this);
        publicRoomClick.setOnClickListener(this);

    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_room_add:
                roomPresenters.showCreateDialog();
                break;
            case R.id.iv_room_setting:
                /*final PopupMenu popupMenu = new PopupMenu(getContext(), ivSetting);
                popupMenu.getMenuInflater().inflate(R.menu.menu_setting, popupMenu.getMenu());
                ivSetting.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {

                                return true;
                            }
                        });
                        popupMenu.show();
                    }
                });*/
                break;
            case R.id.tv_room_logout:
                roomPresenters.logout();
                break;
            case R.id.ln_public_room:
                //startActivity(new Intent(getContext(), HomeActivity.class));

                ChatAllFragment chatAllFragment = new ChatAllFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fl_container, chatAllFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                onShowToast("Click");

                break;
        }
    }

    @Override
    public void getRoomSuccess(ArrayList<String> roomList, int totalRooms) {
        if (totalRooms == 0) {
            tvRoomEmpty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(GONE);
        } else {
            tvRoomEmpty.setVisibility(GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.setRoomAdapter(getContext(), roomList, this);
        }

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void getClickRoom(String roomName) {
        roomPresenters.checkPasswordNull(roomName);
    }

    @Override
    public void onCreateDialog(ArrayList<String> nameList, int size) {
        final ArrayList<String> nList = nameList;
        final ArrayList nameSelected = new ArrayList<>();

        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        final View view = LayoutInflater.from(getContext()).inflate(R.layout.room_create_dialog, null);
        final TextView btnCreate = view.findViewById(R.id.tv_click_confirm);
        final TextView btnCancel = view.findViewById(R.id.tv_click_cancel);
        final EditText edtRoomNameCreate = view.findViewById(R.id.edt_room_name_create);
        final EditText edtRoomPasswordCreate = view.findViewById(R.id.edt_room_password_create);
        final ListView listview = view.findViewById(R.id.lv_nameSelected);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(v);
                return true;
            }
        });

        if (!nList.equals(null)) {
            listview.setAdapter(new ArrayAdapter(getContext(), android.R.layout.simple_list_item_multiple_choice, nList));
            listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    if (listview.isItemChecked(position)) {
                        nameSelected.add(adapterView.getItemAtPosition(position));
                    } else if (nameSelected.contains(adapterView.getItemAtPosition(position))) {
                        nameSelected.remove(adapterView.getItemAtPosition(position));
                    }
                }
            });
        }

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String roomNameCreate = edtRoomNameCreate.getText().toString().trim();
                String roomPasswordCreate = edtRoomPasswordCreate.getText().toString().trim();
                if (roomNameCreate.contentEquals("")) {
                    edtRoomNameCreate.setError("enter room name!");
                /*} else if (!roomNameCreate.matches("[ก-ฮA-Za-z0-9]+")) {
                    edtRoomNameCreate.setError("Only alphabet or number allowed");*/
                } else {
                    roomPresenters.addRoom(roomNameCreate, roomPasswordCreate, nameSelected);
                    dialog.dismiss();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public void onRemoveDialog(final String roomName) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View view = LayoutInflater.from(getContext()).inflate(R.layout.room_remove_dialog, null);

        final TextView btnRemove = view.findViewById(R.id.tv_click_remove);
        final TextView btnCancel = view.findViewById(R.id.tv_click_cancel);

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roomPresenters.removeRoom(roomName);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public void onLogout() {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.room_logout_dialog, null);

        final TextView btnLogout = view.findViewById(R.id.tv_click_logout);
        final TextView btnCancel = view.findViewById(R.id.tv_click_cancel);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(v);
                return true;
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mAuth.signOut();

                LoginFragment loginFragment = new LoginFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.add(R.id.fl_container, loginFragment);
                fragmentTransaction.commit();

                Toast.makeText(getContext(), "Logout na~", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public void onClickRoomSuccess(String roomName) {

        //homeActivity.navigationDismiss();

        ChatFragment chatFragment = new ChatFragment();

        Bundle bundle = new Bundle();
        Parcelable wrapped = Parcels.wrap(roomName);
        bundle.putParcelable("key", wrapped);
        chatFragment.setArguments(bundle);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, chatFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onClickRoomPassword(final String roomName) {
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.room_password_dialog, null);

        final EditText edtRoomPassword = view.findViewById(R.id.edt_room_password);
        final TextView btnConfirm = view.findViewById(R.id.tv_click_confirm);
        final TextView btnCancel = view.findViewById(R.id.tv_click_cancel);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard(v);
                return true;
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String roomPassword = edtRoomPassword.getText().toString().trim();
                roomPresenters.checkRoomPassword(roomName, roomPassword);
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public void get() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.room_password_dialog, null);
        final TextView tvRoomPassWordWrong = view.findViewById(R.id.tv_room_password_incorrect);
        tvRoomPassWordWrong.setVisibility(View.VISIBLE);
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProgressLoad() {
        progressRoomLoad.setVisibility(View.VISIBLE);
    }

    @Override
    public void onProgressDismiss() {
        progressRoomLoad.setVisibility(View.GONE);
    }
}

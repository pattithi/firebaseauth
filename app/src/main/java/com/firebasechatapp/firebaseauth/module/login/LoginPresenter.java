package com.firebasechatapp.firebaseauth.module.login;

import android.content.Context;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.firebasechatapp.firebaseauth.model.UserLogin;
import com.firebasechatapp.firebaseauth.util.Util;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;

public class LoginPresenter extends AppCompatActivity implements LoginConstructor.LoginSetPresenter {

    private LoginConstructor.LoginView loginView;
    private Context context;
    private DatabaseReference database, usersChildDatabase;

    private String deviceToken, currentDeviceId;

    // Realm
    Realm realm;
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public LoginPresenter(LoginConstructor.LoginView loginView, Context context) {
        this.loginView = loginView;
        this.context = context;
        getDevice();
    }

    private void getDevice() {
        deviceToken = FirebaseInstanceId.getInstance().getToken();
        currentDeviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public void userLogin(final String username, final String password) {

        loginView.onLoadShow();

        database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
        usersChildDatabase = database.child("User");

        usersChildDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(username)) {
                    loginView.onTextLoginNotFound();
                    loginView.onLoadDismiss();
                } else if (String.valueOf(dataSnapshot.child(username).child("Password").getValue()).contentEquals(password)) {

                    if (currentDeviceId.contentEquals(String.valueOf(dataSnapshot.child(username).child("DeviceId").getValue())) && deviceToken.contentEquals(String.valueOf(dataSnapshot.child(username).child("DeviceToken").getValue()))) {
                        UserDetails.username = username;
                        UserDetails.password = password;
                    }else{
                        UserDetails.username = username;
                        UserDetails.password = password;
                        usersChildDatabase.child(username).child("DeviceId").setValue(currentDeviceId);
                        usersChildDatabase.child(username).child("DeviceToken").setValue(deviceToken);
                    }

                    saveCurrent();

                    loginView.onLoginSuccess();
                    //loginView.onShowToast("login correct");
                    loginView.onLoadDismiss();
                } else {
                    loginView.onTextLoginPassIncorrect();
                    loginView.onLoadDismiss();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("TAG", "onCancelled", databaseError.toException());
            }
        });
    }

    @Override
    public void googleRegister(String name, String email, String deviceToken, String currentDeviceId) {

        DatabaseReference database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
        DatabaseReference usersChildDatabase = database.child("User");
        DatabaseReference usersChildren = usersChildDatabase.child(name);

        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("Username", name);
        map2.put("Email", UserDetails.email);
        map2.put("DeviceId", currentDeviceId);
        map2.put("DeviceToken", deviceToken);

        usersChildren.updateChildren(map2);

    }

    @Override
    public void clickRegister() {
        loginView.onClickToRegister();
    }

    @Override
    public void setSnackBar(View view) {
        Util.setSnackBar(view, "Login success");
    }

    public void saveCurrent() {
        realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {

            String username = UserDetails.username;

            @Override
            public void execute(Realm realm) {
                UserLogin userLogin = realm.createObject(UserLogin.class); // Create a new object
                userLogin.setName(username);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.i("Database success", "");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e("Database error", error.getMessage());
            }
        });
    }
}

package com.firebasechatapp.firebaseauth.module.chats;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebasechatapp.firebaseauth.R;
import com.firebasechatapp.firebaseauth.model.ChatDetails;
import com.firebasechatapp.firebaseauth.module.chat.ChatAdapter;
import com.firebasechatapp.firebaseauth.module.chat.ChatConstructor;
import com.firebasechatapp.firebaseauth.module.chat.ChatFragment;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;

public class ChatAllFragment extends Fragment implements ChatAllConstructor.ChatAllView, View.OnClickListener, View.OnFocusChangeListener {

    @BindView(R.id.recycler_chat)
    RecyclerView recyclerView;
    Unbinder unbinder;

    @BindView(R.id.tv_room_name)
    TextView tvRoomName;
    @BindView(R.id.tv_chat_head)
    LinearLayout tvRoomHead;
    @BindView(R.id.edt_message)
    EditText edtChatMessage;
    @BindView(R.id.btn_send_message)
    ImageView btnChatSentMessage;
    @BindView(R.id.btn_more_add)
    ImageView btnMoreAdd;
    @BindView(R.id.btn_camera_add)
    ImageView btnCameraAdd;

    private ChatAllAdapter adapter;
    private ChatAllConstructor.ChatAllSetPresenter chatAllPresenters;
    private Dialog dialog;
    private LinearLayoutManager layoutManager;

    private static final int IMAGE_GALLERY_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final String TYPE_MESSAGE_TEXT = "text";

    private ProgressDialog loadingDialog;

    /*public static ChatAllFragment newInstance() {
        return new ChatAllFragment();
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        layoutManager = new LinearLayoutManager(getContext());
        chatAllPresenters = new ChatAllPresenters(this, getContext());
        chatAllPresenters.getMessages();
        adapter = new ChatAllAdapter();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.chat_all_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);

        btnChatSentMessage.setVisibility(View.GONE);

        recyclerView.setOnFocusChangeListener(this);
        edtChatMessage.setOnFocusChangeListener(this);
        btnChatSentMessage.setOnClickListener(this);
        btnMoreAdd.setOnClickListener(this);
        btnCameraAdd.setOnClickListener(this);

        return v;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_send_message:
                String message = edtChatMessage.getText().toString();
                if (!message.equals("")) {
                    chatAllPresenters.sendMessage(message, TYPE_MESSAGE_TEXT);
                    edtChatMessage.setText("");
                }
                break;
            case R.id.btn_more_add:
                photoGalleryIntent();
                break;
            case R.id.btn_camera_add:
                photoCameraIntent();
                break;
        }
    }

    private void photoCameraIntent() {

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 0);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, IMAGE_CAMERA_REQUEST);
            Log.d(TAG, "photoCameraIntent: " + intent);
        }

    }

    private void photoGalleryIntent() {
        try {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, IMAGE_GALLERY_REQUEST);
            } else {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture_title)), IMAGE_GALLERY_REQUEST);

                //Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //startActivityForResult(galleryIntent, IMAGE_GALLERY_REQUEST);
                //Log.d(TAG, "photoGalleryIntent: " + galleryIntent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permission GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_GALLERY_REQUEST) {
            if (resultCode == RESULT_OK) {
                //Log.d(TAG, "Gallery data: " + data + " " + resultCode + " " + RESULT_OK);
                chatAllPresenters.sendPhotoGalleryMessage(requestCode, IMAGE_GALLERY_REQUEST, data);
            }
        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] bytes = baos.toByteArray();

                chatAllPresenters.sendPhotoCameraMessage(requestCode, IMAGE_CAMERA_REQUEST, bytes);
                //Log.d(TAG, "Camera data: " + bytes + " " + resultCode + " " + RESULT_OK);
            }
        }
    }


    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.recycler_chat:
                if (hasFocus) {
                    //onHideKeyboard(view);
                }
                break;
            case R.id.edt_message:
                btnChatSentMessage.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void getMessagesAllSuccess(ArrayList<ChatDetails> chatArrayList) {

        adapter.setChatAllAdapter(getContext(), chatArrayList, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onImageLoadDialog() {
        loadingDialog = new ProgressDialog(getContext());
        loadingDialog.setMessage("Loading...");
        loadingDialog.show();
    }

    @Override
    public void onImageDismissDialog() {
        loadingDialog.dismiss();
    }
}

package com.firebasechatapp.firebaseauth.module.chats;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebasechatapp.firebaseauth.R;
import com.firebasechatapp.firebaseauth.model.ChatDetails;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatAllAdapter extends RecyclerView.Adapter<ChatAllAdapter.viewHolder> {

    private Context context;
    private ArrayList<ChatDetails> dataList;
    private ChatAllConstructor.ChatAllView chatAllView;
    private static final String TYPE_MESSAGE_TEXT = "text";
    private static final String TYPE_MESSAGE_IMAGE = "image";
    private static final String TYPE_MESSAGE_AUDIO = "audio";
    private static final String TYPE_MESSAGE_EMOTICON = "emoticon";

    public void setChatAllAdapter(Context context, ArrayList<ChatDetails> dataList, ChatAllConstructor.ChatAllView chatAllView) {
        this.context = context;
        this.dataList = dataList;
        this.chatAllView = chatAllView;
    }

    @NonNull
    @Override
    public ChatAllAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_all_items, viewGroup, false);
        return new ChatAllAdapter.viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChatAllAdapter.viewHolder viewHolder, int position) {
        final String user = dataList.get(position).getName();
        final String message = dataList.get(position).getMessage();
        final String time = dataList.get(position).getTime();
        final String date = dataList.get(position).getDate();
        final String typeMessage = dataList.get(position).getTypeMessage();
        final int typeUser = dataList.get(position).getType();

        String previousDate = "";
        if (position > 0) {
            previousDate = dataList.get(position - 1).getDate();
        }
        setDateTextVisibility(date, previousDate, viewHolder.textDateTime);

        if (typeUser == 1) {
            viewHolder.layoutMessageLeft.setVisibility(View.GONE);
            viewHolder.layoutMessageRight.setVisibility(View.VISIBLE);
            viewHolder.textNameRight.setText(user);
            if (typeMessage.contentEquals(TYPE_MESSAGE_TEXT)) {
                viewHolder.ivMessageRight.setVisibility(View.GONE);
                viewHolder.layoutSubRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setText(message);
                viewHolder.textMessageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesRight.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesRight.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesRight.setText(time);
                        } else {
                            viewHolder.textTimeMessagesRight.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_IMAGE)) {
                viewHolder.layoutSubRight.setVisibility(View.GONE);
                viewHolder.textMessageRight.setVisibility(View.GONE);
                viewHolder.ivMessageRight.setVisibility(View.VISIBLE);
                Glide.with(context).load(message).into(viewHolder.ivMessageRight);
                viewHolder.ivMessageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesRight.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesRight.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesRight.setText(time);
                        } else {
                            viewHolder.textTimeMessagesRight.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_AUDIO)) {
                viewHolder.ivMessageRight.setVisibility(View.GONE);
                viewHolder.layoutSubRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setText("audio");
                viewHolder.textMessageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesRight.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesRight.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesRight.setText(time);
                        } else {
                            viewHolder.textTimeMessagesRight.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_EMOTICON)) {
                viewHolder.ivMessageRight.setVisibility(View.GONE);
                viewHolder.layoutSubRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setText(message);
                viewHolder.textMessageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesRight.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesRight.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesRight.setText(time);
                        } else {
                            viewHolder.textTimeMessagesRight.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }else if (typeUser == 2) {
            viewHolder.layoutMessageRight.setVisibility(View.GONE);
            viewHolder.layoutMessageLeft.setVisibility(View.VISIBLE);
            viewHolder.textNameLeft.setText(user);
            if (typeMessage.contentEquals(TYPE_MESSAGE_TEXT)) {
                viewHolder.ivMessageLeft.setVisibility(View.GONE);
                viewHolder.layoutSubLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setText(message);
                viewHolder.textMessageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesLeft.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesLeft.setText(time);
                        } else {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_IMAGE)) {
                viewHolder.textMessageLeft.setVisibility(View.GONE);
                viewHolder.ivMessageLeft.setVisibility(View.VISIBLE);
                Glide.with(context).load(message).into(viewHolder.ivMessageLeft);
                viewHolder.layoutSubLeft.setVisibility(View.GONE);
                viewHolder.ivMessageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesLeft.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesLeft.setText(time);
                        } else {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_AUDIO)) {
                viewHolder.ivMessageLeft.setVisibility(View.GONE);
                viewHolder.layoutSubLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setText("audio");
                viewHolder.textMessageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesLeft.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesLeft.setText(time);
                        } else {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_EMOTICON)) {
                viewHolder.ivMessageLeft.setVisibility(View.GONE);
                viewHolder.layoutSubLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setText(message);
                viewHolder.textMessageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesLeft.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesLeft.setText(time);
                        } else {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }

        /*if (typeUser == 1) {
            if (typeMessage.contentEquals(TYPE_MESSAGE_TEXT)) {
                viewHolder.layoutMessageLeft.setVisibility(View.GONE);
                viewHolder.layoutMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textNameRight.setText(user);
                viewHolder.textMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setText(message);
                viewHolder.textMessageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesRight.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesRight.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesRight.setText(time);
                        } else {
                            viewHolder.textTimeMessagesRight.setVisibility(View.GONE);
                        }
                    }
                });
            } else if (typeMessage.contentEquals(TYPE_MESSAGE_IMAGE)) {
                viewHolder.layoutMessageLeft.setVisibility(View.GONE);
                viewHolder.layoutMessageRight.setVisibility(View.VISIBLE);
                viewHolder.layoutSubRight.setVisibility(View.GONE);
                viewHolder.textNameRight.setText(user);
                viewHolder.ivMessageRight.setVisibility(View.VISIBLE);
                Glide.with(context).load(message).into(viewHolder.ivMessageRight);
                viewHolder.ivMessageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesRight.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesRight.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesRight.setText(time);
                        } else {
                            viewHolder.textTimeMessagesRight.setVisibility(View.GONE);
                        }
                    }
                });
            } else if (typeMessage.contentEquals(TYPE_MESSAGE_AUDIO)) {
                viewHolder.layoutMessageLeft.setVisibility(View.GONE);
                viewHolder.layoutMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textNameRight.setText(user);
                viewHolder.textMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setText("audio");
                viewHolder.textMessageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesRight.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesRight.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesRight.setText(time);
                        } else {
                            viewHolder.textTimeMessagesRight.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_EMOTICON)) {
                viewHolder.layoutMessageLeft.setVisibility(View.GONE);
                viewHolder.layoutMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textNameRight.setText(user);
                viewHolder.textMessageRight.setVisibility(View.VISIBLE);
                viewHolder.textMessageRight.setText("emoticon");
                viewHolder.textMessageRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesRight.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesRight.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesRight.setText(time);
                        } else {
                            viewHolder.textTimeMessagesRight.setVisibility(View.GONE);
                        }
                    }
                });
            }


        } else {
            if (typeMessage.contentEquals(TYPE_MESSAGE_TEXT)) {
                viewHolder.layoutMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.layoutMessageRight.setVisibility(View.GONE);
                viewHolder.textNameLeft.setText(user);
                viewHolder.textMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setText(message);
                viewHolder.textMessageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesLeft.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesLeft.setText(time);
                        } else {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.GONE);
                        }
                    }
                });
            } else if (typeMessage.contentEquals(TYPE_MESSAGE_IMAGE)) {
                viewHolder.layoutMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.layoutMessageRight.setVisibility(View.GONE);
                viewHolder.layoutSubLeft.setVisibility(View.GONE);
                viewHolder.textNameLeft.setText(user);
                viewHolder.ivMessageLeft.setVisibility(View.VISIBLE);
                Glide.with(context).load(message).into(viewHolder.ivMessageLeft);
                viewHolder.ivMessageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesLeft.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesLeft.setText(time);
                        } else {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_AUDIO)) {
                viewHolder.layoutMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.layoutMessageRight.setVisibility(View.GONE);
                viewHolder.textNameLeft.setText(user);
                viewHolder.textMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setText("audio");
                viewHolder.textMessageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesLeft.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesLeft.setText(time);
                        } else {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.GONE);
                        }
                    }
                });
            }else if (typeMessage.contentEquals(TYPE_MESSAGE_EMOTICON)) {
                viewHolder.layoutMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.layoutMessageRight.setVisibility(View.GONE);
                viewHolder.textNameLeft.setText(user);
                viewHolder.textMessageLeft.setVisibility(View.VISIBLE);
                viewHolder.textMessageLeft.setText("emoticon");
                viewHolder.textMessageLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (viewHolder.textTimeMessagesLeft.getVisibility() == View.GONE) {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.VISIBLE);
                            viewHolder.textTimeMessagesLeft.setText(time);
                        } else {
                            viewHolder.textTimeMessagesLeft.setVisibility(View.GONE);
                        }
                    }
                });
            }
        }*/

    }

    private void setDateTextVisibility(String currentDate, String previousDate, TextView textDateTime) {

        if (previousDate.isEmpty()) {
            textDateTime.setVisibility(View.VISIBLE);
            textDateTime.setText(currentDate);
        } else {
            if (currentDate.contentEquals(previousDate)) {
                textDateTime.setVisibility(View.GONE);
                textDateTime.setText("");
            } else {
                textDateTime.setVisibility(View.VISIBLE);
                textDateTime.setText(currentDate);
            }
        }

    }

    @Override
    public int getItemCount() {
        if (dataList == null)
            return 0;
        if (dataList.isEmpty())
            return 0;
        return dataList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_date_time)
        TextView textDateTime;

        @BindView(R.id.iv_message_left)
        ImageView ivMessageLeft;
        @BindView(R.id.tv_chat_name_in)
        TextView textNameLeft;
        @BindView(R.id.tv_message_left)
        TextView textMessageLeft;
        @BindView(R.id.tv_message_time_left)
        TextView textTimeMessagesLeft;
        @BindView(R.id.ln_message_left)
        LinearLayout layoutMessageLeft;
        @BindView(R.id.ln_sub_chat_left)
        LinearLayout layoutSubLeft;

        @BindView(R.id.iv_message_right)
        ImageView ivMessageRight;
        @BindView(R.id.tv_chat_name_out)
        TextView textNameRight;
        @BindView(R.id.tv_message_time_right)
        TextView textTimeMessagesRight;
        @BindView(R.id.tv_message_right)
        TextView textMessageRight;
        @BindView(R.id.ln_message_right)
        LinearLayout layoutMessageRight;
        @BindView(R.id.ln_sub_chat_right)
        LinearLayout layoutSubRight;

        @BindView(R.id.ln_chat_item)
        LinearLayout layoutChatItem;

        public viewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}

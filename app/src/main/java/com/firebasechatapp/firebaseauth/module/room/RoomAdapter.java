package com.firebasechatapp.firebaseauth.module.room;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebasechatapp.firebaseauth.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.viewHolder> {

    private Context context;
    private ArrayList<String> dataList;
    private RoomConstructor.RoomView roomView;

    public void setRoomAdapter(Context context, ArrayList<String> dataList,  RoomConstructor.RoomView roomView) {
        this.context = context;
        this.dataList = dataList;
        this.roomView = roomView;
    }

    @NonNull
    @Override
    public RoomAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.room_items, viewGroup, false);
        return new RoomAdapter.viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RoomAdapter.viewHolder viewHolder, final int position) {

        final String roomName = dataList.get(position);
        final String[] splitStr = roomName.trim().split("\\s+");

        viewHolder.tvRoomName.setText(splitStr[0]);
        Glide.with(context).load(splitStr[1]).into(viewHolder.roomPic);
        viewHolder.layoutRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                roomView.getClickRoom(splitStr[0]);
            }
        });

        viewHolder.layoutRoom.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                roomView.onRemoveDialog(splitStr[0]);
                return true;
            }
        });


    }

    @Override
    public int getItemCount() {
        if (dataList == null)
            return 0;
        if (dataList.isEmpty())
            return 0;
        return dataList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_room_name)
        TextView tvRoomName;
        @BindView(R.id.room_picture)
        ImageView roomPic;
        @BindView(R.id.ln_public_room)
        LinearLayout layoutRoom;

        public viewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }

}

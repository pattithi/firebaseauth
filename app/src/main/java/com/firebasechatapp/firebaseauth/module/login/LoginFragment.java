package com.firebasechatapp.firebaseauth.module.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebasechatapp.firebaseauth.R;
import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.firebasechatapp.firebaseauth.model.UserRegisterDetails;
import com.firebasechatapp.firebaseauth.module.register.RegisterFragment;
import com.firebasechatapp.firebaseauth.module.room.RoomFragment;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.firebasechatapp.firebaseauth.model.UserDetails.email;

public class LoginFragment extends Fragment implements LoginConstructor.LoginView, View.OnClickListener, View.OnFocusChangeListener {

    Unbinder unbinder;
    @BindView(R.id.tv_login_user_found)
    TextView tvUserNotFound;
    @BindView(R.id.tv_login_password_incorrect)
    TextView tvPasswordIncorrect;
    @BindView(R.id.edt_login_username)
    EditText edtUsername;
    @BindView(R.id.edt_login_password)
    EditText edtPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btnGoogle)
    SignInButton btnGoogleLogin;
    @BindView(R.id.tv_click_register)
    TextView btnClickToRegister;
    @BindView(R.id.ln_login)
    LinearLayout lnLogin;
    ProgressDialog loadingDialog;

    //EditText val
    private String username, password;

    //Login Presenter
    private LoginConstructor.LoginSetPresenter loginPresenters;

    //Google configure
    private FirebaseAuth mAuth;
    private GoogleApiClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 2;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private static final String TAG = LoginFragment.class.getSimpleName();
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleSignInClient != null && mGoogleSignInClient.isConnected()) {
            mGoogleSignInClient.stopAutoManage(getActivity());
            mGoogleSignInClient.disconnect();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        googleConfigure();

        loginPresenters = new LoginPresenter(this, getContext());

    }

    @Override
    public void onResume() {
        super.onResume();

        if (!UserRegisterDetails.username.isEmpty() && !UserRegisterDetails.password.isEmpty()) {
            edtUsername.setText(UserRegisterDetails.username);
            edtPassword.setText(UserRegisterDetails.password);
            loginPresenters.userLogin(UserRegisterDetails.username, UserRegisterDetails.password);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lnLogin.setOnFocusChangeListener(this);
        btnLogin.setOnClickListener(this);
        btnGoogleLogin.setOnClickListener(this);
        btnClickToRegister.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                username = edtUsername.getText().toString().trim();
                password = edtPassword.getText().toString().trim();
                if (username.contentEquals("")) {
                    edtUsername.setError("enter username blank");
                } else if (password.contentEquals("")) {
                    edtPassword.setError("enter password blank");
                } else {
                    tvUserNotFound.setVisibility(View.GONE);
                    tvPasswordIncorrect.setVisibility(View.GONE);
                    loginPresenters.userLogin(username, password);
                    loginPresenters.setSnackBar(view);
                }
                break;
            case R.id.btnGoogle:
                signIn();
                break;
            case R.id.tv_click_register:
                loginPresenters.clickRegister();
                break;
        }
    }

    //Google login config
    private void googleConfigure() {
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage(getActivity(), new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void signIn() {
        Auth.GoogleSignInApi.signOut(mGoogleSignInClient);
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleSignInClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
                Log.e("Test ", "Google Auth");
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());
        onLoadShow();

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(LoginFragment.this.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                String name = user.getDisplayName().toString();
                                String email = user.getEmail().toString();

                                final String[] splitName = name.trim().split("\\s+");

                                UserDetails.username = splitName[0];
                                UserDetails.email = email;

                                // Google register
                                String deviceToken = FirebaseInstanceId.getInstance().getToken();
                                String currentDeviceId = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                                loginPresenters.googleRegister(splitName[0], email, deviceToken, currentDeviceId);

                                RoomFragment roomFragment = new RoomFragment();
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.fl_container, roomFragment);
                                fragmentTransaction.commit();

                                onLoadDismiss();
                            }
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            //Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
            hideSoftKeyboard(view);
        }
    }

    private void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onLoginSuccess() {
        edtUsername.setText("");
        edtPassword.setText("");

        RoomFragment roomFragment = new RoomFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, roomFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClickToRegister() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, RegisterFragment.newInstance());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoadShow() {
        loadingDialog = new ProgressDialog(getContext());
        loadingDialog.setMessage("Loading...");
        loadingDialog.show();
    }

    @Override
    public void onLoadDismiss() {
        loadingDialog.dismiss();
    }

    @Override
    public void onTextLoginNotFound() {
        tvUserNotFound.setVisibility(View.VISIBLE);
        edtUsername.setText("");
        edtPassword.setText("");
    }

    @Override
    public void onTextLoginPassIncorrect() {
        tvPasswordIncorrect.setVisibility(View.VISIBLE);
        edtPassword.setText("");
    }
}

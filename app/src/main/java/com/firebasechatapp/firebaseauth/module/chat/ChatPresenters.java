package com.firebasechatapp.firebaseauth.module.chat;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.format.DateFormat;
import android.util.Log;

import com.firebasechatapp.firebaseauth.model.ChatDetails;
import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.firebasechatapp.firebaseauth.util.Util;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.HttpHeaders;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ChatPresenters implements ChatConstructor.ChatSetPresenter {

    private ChatConstructor.ChatView chatView;
    private Context context;
    private DatabaseReference database;
    private DatabaseReference roomChildDatabase;
    private DatabaseReference roomNameChildDatabase;
    private DatabaseReference messageChildDatabase;
    private ArrayList<ChatDetails> chatArrayList = new ArrayList<>();
    private ArrayList<String> friendList = new ArrayList<>();
    private String roomName;
    private JSONArray registration_ids = new JSONArray();
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private static final String TAG = ChatPresenters.class.getSimpleName();
    private static final String TYPE_MESSAGE_IMAGE = "image";
    private StorageReference storageRef = storage.getReferenceFromUrl(Util.URL_STORAGE_REFERENCE).child(Util.FOLDER_STORAGE_IMG);
    //////////////////////////////////////

    public ChatPresenters(ChatConstructor.ChatView chatView, Context context, String roomName) {
        this.chatView = chatView;
        this.context = context;
        this.roomName = roomName;
    }


    @Override
    public void getMessages(String roomName) {
        database = FirebaseDatabase.getInstance().getReference(); // หลัก
        roomChildDatabase = database.child("Room");
        roomNameChildDatabase = roomChildDatabase.child(roomName);
        messageChildDatabase = roomNameChildDatabase.child("Messages");

        /*messageChildDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //Log.e("DataSnapshot", String.valueOf(dataSnapshot.getValue()));
                String date = String.valueOf(dataSnapshot.child("date").getValue());
                String message = String.valueOf(dataSnapshot.child("message").getValue());
                String time = String.valueOf(dataSnapshot.child("time").getValue());
                String user = String.valueOf(dataSnapshot.child("user").getValue());
                String typeMessage = String.valueOf(dataSnapshot.child("type_message").getValue());

                if (user.contentEquals(UserDetails.username)) {
                    chatArrayList.add(new ChatDetails(UserDetails.username, message, time, date, typeMessage, 1));
                    chatView.getMessagesSuccess(chatArrayList);
                } else {
                    chatArrayList.add(new ChatDetails(user, message, time, date, typeMessage, 2));
                    chatView.getMessagesSuccess(chatArrayList);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });*/

        messageChildDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    String date = String.valueOf(messageSnapshot.child("date").getValue());
                    String message = String.valueOf(messageSnapshot.child("message").getValue());
                    String time = String.valueOf(messageSnapshot.child("time").getValue());
                    String user = String.valueOf(messageSnapshot.child("user").getValue());
                    String typeMessage = String.valueOf(messageSnapshot.child("type_message").getValue());

                    if (user.equals(UserDetails.username)) {
                        //Log.e("Chat Pre", message + " " + date + " " + time + " " + user + " " + typeMessage);
                        chatArrayList.add(new ChatDetails(UserDetails.username, message, time, date, typeMessage, 1));
                        chatView.getMessagesSuccess(chatArrayList);
                    } else {
                        chatArrayList.add(new ChatDetails(user, message, time, date, typeMessage, 2));
                        chatView.getMessagesSuccess(chatArrayList);
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void sendMessage(final String message, final String roomName, final String type_message) {

        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");
        roomNameChildDatabase = roomChildDatabase.child(roomName);
        DatabaseReference userChildDatabase = roomNameChildDatabase.child("User");

        userChildDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                friendList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!String.valueOf(snapshot.getValue()).contentEquals(UserDetails.username)) {
                        friendList.add(String.valueOf(snapshot.getValue()));
                    }
                }
                //Log.e(TAG, "Set friend" + String.valueOf(friendList));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        DatabaseReference userRefChildDatabase = database.child("User");
        final ValueEventListener userValueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                registration_ids = new JSONArray();
                for (int i = 0; i < friendList.size(); i++) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (String.valueOf(snapshot.child("Username").getValue()).contentEquals(String.valueOf(friendList.get(i)))) {
                            registration_ids.put(String.valueOf(snapshot.child("DeviceToken").getValue()));
                            //Log.e(TAG, friendList.get(i));
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                registration_ids = new JSONArray();
            }
        };
        userRefChildDatabase.addValueEventListener(userValueEventListener);


        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        //dateFormat = new SimpleDateFormat("EEE, MMM dd, yyyy");
        String currentTime = timeFormat.format(new Date());

        String date = String.valueOf(Calendar.getInstance().get(Calendar.DATE));
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int year = Calendar.getInstance().get(Calendar.YEAR);

        String monthPlus = String.valueOf(month+1);
        String yearPlus = String.valueOf(year+543);

        String currentDate = date+"/"+monthPlus+"/"+yearPlus;

        Map<String, Object> map = new HashMap<String, Object>();
        String temp_key = messageChildDatabase.push().getKey();
        messageChildDatabase.updateChildren(map);

        DatabaseReference message_root = messageChildDatabase.child(temp_key);
        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("user", UserDetails.username);
        map2.put("date", currentDate);
        map2.put("time", currentTime);
        map2.put("message", message);
        map2.put("type_message", type_message);

        message_root.updateChildren(map2, new DatabaseReference.CompletionListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError != null) {
                    //Log.i(TAG, databaseError.toString());
                } else {
                    if (registration_ids.length() > 0) {
                        //Log.d(TAG, String.valueOf(databaseReference));
                        String url = "https://fcm.googleapis.com/fcm/send";
                        AsyncHttpClient client = new AsyncHttpClient();

                        client.addHeader(HttpHeaders.AUTHORIZATION, "key=AIzaSyBE_svX7kJjOHXRhRKu1tSceiMDQyq7NRg");
                        client.addHeader(HttpHeaders.CONTENT_TYPE, RequestParams.APPLICATION_JSON);

                        try {
                            JSONObject params = new JSONObject();
                            params.put("registration_ids", registration_ids);

                            JSONObject notificationObject = new JSONObject();
                            notificationObject.put("body", message);
                            notificationObject.put("title", UserDetails.username);

                            Log.e(TAG, String.valueOf(notificationObject));

                            params.put("notification", notificationObject);

                            StringEntity entity = new StringEntity(params.toString(), StandardCharsets.UTF_8);

                            client.post(context, url, entity, RequestParams.APPLICATION_JSON, new TextHttpResponseHandler() {
                                @Override
                                public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                                    Log.i(TAG, responseString);
                                }

                                @Override
                                public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                                    Log.i(TAG, responseString);
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        chatView.showToast("Send na ~~");

    }

    @Override
    public void clickShowFriend() {
        chatView.onClickToFriendList();
    }

    @Override
    public void clickSetting() {
        chatView.onSettingDialog(roomName);
    }

    @Override
    public void editNewName(String roomName, String newPassRoom) {

        database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
        roomChildDatabase = database.child("Room");
        DatabaseReference roomNameChildren = roomChildDatabase.child(roomName);

        Map newPassData = new HashMap();
        newPassData.put("Password", newPassRoom);

        roomNameChildren.updateChildren(newPassData);

        chatView.showToast("Password changed");

    }

    @Override
    public void editNewPic(String roomName, String newPicRoom) {
        database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
        roomChildDatabase = database.child("Room");
        DatabaseReference roomNameChildren = roomChildDatabase.child(roomName);

        Map newPicData = new HashMap();
        newPicData.put("Img", newPicRoom);

        roomNameChildren.updateChildren(newPicData);

        chatView.showToast("Picture changed");

    }

    @Override
    public void sendPhotoGalleryMessage(int requestCode, int IMAGE_GALLERY_REQUEST, Intent data) {

        chatView.onImageLoadDialog();

        Uri selectedImageUri = data.getData();
        if (selectedImageUri != null) {

            if (storageRef != null) {
                final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
                final StorageReference imageGalleryRef = storageRef.child(name + "_gallery");
                UploadTask uploadTask = imageGalleryRef.putFile(selectedImageUri);
                uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return imageGalleryRef.getDownloadUrl();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        //Log.e("OK NA", String.valueOf(uri));
                        sendMessage(String.valueOf(uri), roomName, TYPE_MESSAGE_IMAGE);
                        chatView.onImageDismissDialog();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

            } else {
                //IS NULL
            }

        } else {
            //URI IS NULL
        }

    }

    @Override
    public void sendPhotoCameraMessage(int requestCode, int IMAGE_GALLERY_REQUEST, byte[] data) {

        chatView.onImageLoadDialog();
        if (data != null) {
            if (storageRef != null) {
                final String name = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
                final StorageReference imageGalleryRef = storageRef.child(name + "_camera");
                UploadTask uploadTask = imageGalleryRef.putBytes(data);
                uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return imageGalleryRef.getDownloadUrl();
                    }
                }).addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        //Log.e("OK NA", String.valueOf(uri));
                        sendMessage(String.valueOf(uri), roomName, TYPE_MESSAGE_IMAGE);
                        chatView.onImageDismissDialog();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            } else {

            }
        } else {

        }
    }
}

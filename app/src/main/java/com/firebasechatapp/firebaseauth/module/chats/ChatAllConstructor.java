package com.firebasechatapp.firebaseauth.module.chats;

import android.content.Intent;

import com.firebasechatapp.firebaseauth.model.ChatDetails;

import java.util.ArrayList;

public class ChatAllConstructor{

    public interface ChatAllSetPresenter{
        void getMessages();
        void sendMessage(String message, String TYPE_MESSAGE_TEXT);
        void sendPhotoGalleryMessage(int requestCode, int IMAGE_GALLERY_REQUEST, Intent data);
        void sendPhotoCameraMessage(int requestCode, int IMAGE_GALLERY_REQUEST, byte[] data);
    }

    public interface ChatAllView{
        void getMessagesAllSuccess(ArrayList<ChatDetails> chatArrayList);
        void onImageLoadDialog();
        void onImageDismissDialog();

    }

}

package com.firebasechatapp.firebaseauth.module.chat.friend;

import java.util.ArrayList;

public class FriendConstructor {
    public interface FriendSetPresenter {
        void getFriend(String roomName);
        void clickShowFriend();
        void setAddRoomFriend(ArrayList<String> nameSelected, String roomName);
        void removeFriend(String friendName, String roomName);
    }

    public interface FriendView {
        void showToast(String message);
        void getFriendSuccess(ArrayList<String> friendList, int totalFriend);
        void onAddFriendDialog(ArrayList<String> nameList, int size);
        void onRemoveDialog(String friendName);

    }
}

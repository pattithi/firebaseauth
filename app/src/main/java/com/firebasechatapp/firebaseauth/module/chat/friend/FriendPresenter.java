package com.firebasechatapp.firebaseauth.module.chat.friend;


import android.support.annotation.NonNull;
import android.util.Log;

import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class FriendPresenter implements FriendConstructor.FriendSetPresenter {

    private FriendConstructor.FriendView friendView;
    private DatabaseReference database, usersChildDatabase, roomChildDatabase, roomsNameChildDatabase, roomNameChildren, roomMemberChildren, roomMemberRemove;
    private ArrayList<String> friendList = new ArrayList<>();
    private ArrayList<String> friendImgList = new ArrayList<>();
    private DatabaseReference usersNameChildDatabase;

    public FriendPresenter(FriendConstructor.FriendView friendView) {
        this.friendView = friendView;
    }

    @Override
    public void getFriend(String roomName) {

        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");
        roomsNameChildDatabase = roomChildDatabase.child(roomName);
        roomMemberChildren = roomsNameChildDatabase.child("User");
        roomMemberChildren.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                friendList.clear();
                friendImgList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    friendList.add(String.valueOf(snapshot.getValue()));
                }
                //friendView.getFriendSuccess(friendList, friendList.size());

                for(int i=0;i<friendList.size();i++){
                    usersChildDatabase = database.child("User");
                    usersNameChildDatabase = usersChildDatabase.child(friendList.get(i));
                    usersNameChildDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            //Log.e("FriendPre", String.valueOf(dataSnapshot.child("Username").getValue()) + " " + String.valueOf(dataSnapshot.child("Img").getValue()));
                            friendImgList.add(String.valueOf(dataSnapshot.child("Username").getValue()) + " " + String.valueOf(dataSnapshot.child("Img").getValue()));
                            friendView.getFriendSuccess(friendImgList, friendImgList.size());
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void clickShowFriend() {
        database = FirebaseDatabase.getInstance().getReference();
        usersChildDatabase = database.child("User");

        usersChildDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                friendList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!snapshot.getKey().toString().contentEquals(UserDetails.username)) {
                        //friendList.add(String.valueOf(snapshot.getKey()));
                        friendList.add(String.valueOf(snapshot.child("Username").getValue()));
                    }
                }
                //friendView.onAddFriendDialog(friendList, friendList.size());
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        friendView.onAddFriendDialog(friendList, friendList.size());
    }

    @Override
    public void setAddRoomFriend(ArrayList<String> nameSelected, String roomName) {

        Map<String, Object> map = new HashMap<>();
        map.put(UserDetails.username, UserDetails.username);

        for (int i = 0; i < nameSelected.size(); i++) {
            map.put((String) nameSelected.get(i), (String) nameSelected.get(i));
        }

        database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
        roomChildDatabase = database.child("Room");
        roomNameChildren = roomChildDatabase.child(roomName);
        roomMemberChildren = roomNameChildren.child("User");

        roomMemberChildren.updateChildren(map);


    }

    @Override
    public void removeFriend(String friendName, String roomName) {

        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");
        roomNameChildren = roomChildDatabase.child(roomName);
        roomMemberChildren = roomNameChildren.child("User");

        roomMemberRemove = roomMemberChildren.child(friendName);

        roomMemberRemove.removeValue();

        Log.e("LOL", String.valueOf(roomMemberChildren));


    }
}
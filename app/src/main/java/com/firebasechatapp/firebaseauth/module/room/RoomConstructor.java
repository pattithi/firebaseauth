package com.firebasechatapp.firebaseauth.module.room;

import java.util.ArrayList;

public class RoomConstructor {
    public interface RoomSetPresenter {
        void getRoom();
        void addRoom(String roomNameCreate, String roomPasswordCreate, ArrayList<String> nameSelected);
        void removeRoom(String roomNameRemove);
        void showCreateDialog();
        void logout();
        void checkPasswordNull(String roomName);
        void checkRoomPassword(String roomName, String roomPassword);

    }

    public interface RoomView {
        void getRoomSuccess(ArrayList<String> roomList, int totalRooms);
        void getClickRoom(String roomName);
        void onCreateDialog(ArrayList<String> nameList, int size);
        void onRemoveDialog(String roomName);
        void onLogout();
        void onClickRoomSuccess(String roomName);
        void onClickRoomPassword(String roomName);
        void get();
        void onShowToast(String message);
        void onProgressLoad();
        void onProgressDismiss();
    }
}

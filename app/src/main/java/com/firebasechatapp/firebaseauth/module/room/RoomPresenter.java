package com.firebasechatapp.firebaseauth.module.room;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.firebasechatapp.firebaseauth.model.UserLogin;

import com.firebasechatapp.firebaseauth.model.UserRegisterDetails;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

public class RoomPresenter implements RoomConstructor.RoomSetPresenter, RoomRealmData {

    RoomConstructor.RoomView roomView;
    private DatabaseReference database, roomChildDatabase, roomNameChildren, roomMemberChildren;
    private ArrayList<String> roomList = new ArrayList<>();
    Realm realm;
    private ArrayList<String> friendList = new ArrayList<>();
    private String currentTime;
    private SimpleDateFormat simpleDateFormat;

    private static final String TAG = RoomPresenter.class.getSimpleName();
    //////////////////////////////////////

    public RoomPresenter(RoomConstructor.RoomView roomView) {
        this.roomView = roomView;
        //test();
        //addListenerAll();
        //addListenerCheck();
    }

    @Override
    public void getRoom() {
        roomView.onProgressLoad();
        final Map<String, String> roomMap = new HashMap<>();
        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");
        roomChildDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                roomList.clear();       //Clear data before set adapt.
                roomMap.clear();          //Clear data before set adapt.
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {       //Find room loop.
                    if (snapshot.child("User").hasChild(UserDetails.username)) {     // If room have child name contain your name.
                        //roomList.add(snapshot.getKey());    //If found. add room name to array.
                        if (snapshot.hasChild("TimeCreate")) {
                            //roomMap.put(snapshot.getKey(), (String) snapshot.child("TimeCreate").getValue());     //Add room name and time create to map
                            roomMap.put(snapshot.getKey() + " " + (String) snapshot.child("Img").getValue(), (String) snapshot.child("TimeCreate").getValue());     //Add room name and time create to map
                        } else {
                            roomMap.put(snapshot.getKey() + " " + (String) snapshot.child("Img").getValue(), (String) snapshot.child("TimeCreate").getValue());     //Add room name and time create to map
                            //roomMap.put(snapshot.getKey(), "");     //Add room name and time create to map
                        }
                    } else {
                        //Log.i(TAG, "another room");
                    }
                }

                if(!roomMap.isEmpty()) {
                    Object[] preSortObject = roomMap.entrySet().toArray();
                    if(!preSortObject.toString().isEmpty()) {
                        Arrays.sort(preSortObject, new Comparator() {
                            public int compare(Object o1, Object o2) {
                                return ((Map.Entry<String, String>) o2).getValue().compareTo(((Map.Entry<String, String>) o1).getValue());
                            }
                        });
                        for (Object lastSortObject : preSortObject) {
                            roomList.add(((Map.Entry<String, String>) lastSortObject).getKey());
                        }
                    }
                }
                roomView.getRoomSuccess(roomList, roomList.size());
                roomView.onProgressDismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /*private void test() {
        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");
        roomChildDatabase
                .orderByChild("User")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            if (snapshot.child("User").hasChild(UserDetails.username)) {

                                Iterator iterator = dataSnapshot.getChildren().iterator();
                                Set<String> set = new HashSet<String>();
                                while (iterator.hasNext()) {
                                    set.add((String) ((DataSnapshot) iterator.next()).getKey());
                                }
                                roomList.clear();
                                roomList.addAll(set);
                                roomView.getRoomSuccess(roomList, set.size());
                            }
                        }

                        Log.e("d", String.valueOf(dataSnapshot));
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void addListenerCheck() {

        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");

        *//*Query query = database.child("Room").orderByKey();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        getKey(issue.getKey());
                        //Log.e("AAA: ", String.valueOf(issue.getKey()));
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*//*

        roomChildDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                final String key = dataSnapshot.getKey();
                getKey(key);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                String data = dataSnapshot.getKey();
                for (int i = 0; i < roomList.size(); i++) {
                    if (data.contentEquals(roomList.get(i))) {
                        roomList.remove(i);
                    }
                }
                roomView.getRoomSuccess(roomList, roomList.size());
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addListenerAll() {

        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");

        roomChildDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Iterator iterator = dataSnapshot.getChildren().iterator();
                Set<String> set = new HashSet<String>();
                while (iterator.hasNext()) {
                    String r = (String) ((DataSnapshot) iterator.next()).getKey();
                    set.add(r);
                }

                roomList.clear();
                roomList.addAll(set);
                roomView.getRoomSuccess(roomList, set.size());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }*/


    @SuppressLint("SimpleDateFormat")
    @Override
    public void addRoom(final String roomNameCreate, final String roomPasswordCreate, final ArrayList nameSelected) {

        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        currentTime = simpleDateFormat.format(new Date());

        final Map<String, Object> map = new HashMap<String, Object>();
        map.put("Password", roomPasswordCreate);
        map.put("TimeCreate", currentTime);

        final Map<String, Object> map2 = new HashMap<>();
        map2.put(UserDetails.username, UserDetails.username);

        for (int i = 0; i < nameSelected.size(); i++) {
            map2.put((String) nameSelected.get(i), (String) nameSelected.get(i));
        }

        database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
        roomChildDatabase = database.child("Room");
        roomNameChildren = roomChildDatabase.child(roomNameCreate);
        roomMemberChildren = roomNameChildren.child("User");

        roomNameChildren.updateChildren(map);
        roomMemberChildren.updateChildren(map2);

        /*database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
        roomChildDatabase = database.child("Room");

        roomChildDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(roomNameCreate)) {
                    database = FirebaseDatabase.getInstance().getReference(); // หลัก ๆ
                    roomChildDatabase = database.child("Room");
                    roomNameChildren = roomChildDatabase.child(roomNameCreate);
                    roomMemberChildren = roomNameChildren.child("User");
                    roomNameChildren.updateChildren(map);
                    roomMemberChildren.updateChildren(map2);
                } else {
                    Log.e(TAG, "Room Already");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/

    }

    @Override
    public void removeRoom(String roomNameRemove) {

        roomChildDatabase = database.child("Room");
        roomNameChildren = roomChildDatabase.child(roomNameRemove);
        roomNameChildren.removeValue();

        /*database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");
        roomNameChildren = roomChildDatabase.child(roomNameRemove);

        roomNameChildren.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot appleSnapshot : dataSnapshot.getChildren()) {
                    appleSnapshot.getRef().removeValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });*/
    }

    @Override
    public void logout() {
        UserRegisterDetails.username = "";
        UserRegisterDetails.password = "";
        UserDetails.username = "";
        UserDetails.password = "";

        removeCurrent();
        roomView.onLogout();
    }

    @Override
    public void checkPasswordNull(final String roomName) {
        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");
        roomNameChildren = roomChildDatabase.child(roomName);

        roomNameChildren.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (String.valueOf(dataSnapshot.child("Password").getValue()).contentEquals("")) {
                    roomView.onClickRoomSuccess(roomName);
                } else {
                    roomView.onClickRoomPassword(roomName);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void checkRoomPassword(final String roomName, final String roomPassword) {
        database = FirebaseDatabase.getInstance().getReference();
        roomChildDatabase = database.child("Room");
        roomNameChildren = roomChildDatabase.child(roomName);

        roomNameChildren.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (String.valueOf(dataSnapshot.child("Password").getValue()).contentEquals(roomPassword)) {
                    roomView.onClickRoomSuccess(roomName);
                } else {
                    roomView.get();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void showCreateDialog() {
        database = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersChildDatabase = database.child("User");

        usersChildDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                friendList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!snapshot.getKey().toString().contentEquals(UserDetails.username)) {
                        friendList.add(String.valueOf(snapshot.child("Username").getValue()));
                    }
                    //Log.e("CCCCCCCCC", String.valueOf(snapshot.child("Username").getValue()));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        roomView.onCreateDialog(friendList, friendList.size());

    }


    @Override
    public void removeCurrent() {
        realm = Realm.getDefaultInstance();
        final RealmResults<UserLogin> persons = realm.where(UserLogin.class).findAll();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                persons.deleteAllFromRealm();
            }
        });
        Log.i(TAG, "RealmLogout = " + "Logout success");
    }

    /*public void getKey(final String key) {

        database = FirebaseDatabase.getInstance().getReference();

        *//*Query query = roomChildDatabase.child(key).child("User").orderByKey().equalTo(UserDetails.username);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Log.e("AAA: ", String.valueOf(roomList));
                roomList.add(roomChildDatabase.child(key).getKey());
                roomView.getRoomSuccess(roomList, roomList.size());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*//*

        roomChildDatabase.child(key).child("User").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String makeBy = dataSnapshot.getKey();
                if (makeBy.equals(UserDetails.username)) {

                    //roomList.clear();
                    roomList.add(roomChildDatabase.child(key).getKey());
                    roomView.getRoomSuccess(roomList, roomList.size());

                    //Log.e("makeAA", String.valueOf(roomChildDatabase.child(key).getKey()));
                } else {
                    //Log.e("makeB", String.valueOf(roomChildDatabase.child(key).getKey()));

                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }*/
}


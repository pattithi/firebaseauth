package com.firebasechatapp.firebaseauth.module.chat.friend;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebasechatapp.firebaseauth.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.viewHolder> {

    private Context context;
    private ArrayList<String> dataList;
    private FriendConstructor.FriendView friendView;

    public void setFriendAdapter(Context context, ArrayList<String> dataList, FriendConstructor.FriendView friendView) {
        this.context = context;
        this.dataList = dataList;
        this.friendView = friendView;
    }

    @NonNull
    @Override
    public FriendAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.friend_items, viewGroup, false);
        return new FriendAdapter.viewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendAdapter.viewHolder viewHolder, final int position) {

        final String friendName = dataList.get(position);
        final String[] splitStr = friendName.trim().split("\\s+");

        viewHolder.tvFriendName.setText(splitStr[0]);
        Glide.with(context).load(splitStr[1]).into(viewHolder.friendPic);

        viewHolder.layoutFriend.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                friendView.onRemoveDialog(splitStr[0]);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        if (dataList == null)
            return 0;
        if (dataList.isEmpty())
            return 0;
        return dataList.size();
    }

    public static class viewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_friend_name)
        TextView tvFriendName;
        @BindView(R.id.friend_picture)
        ImageView friendPic;
        @BindView(R.id.ln_friend)
        LinearLayout layoutFriend;

        public viewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }
}

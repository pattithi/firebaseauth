package com.firebasechatapp.firebaseauth.module.register;

public class RegisterConstructor {
    public interface RegisterSetPresenter {
        void userRegister(String username, String password, String deviceToken, String currentDeviceId);
        void clickLogin();
    }

    public interface RegisterView {
        void showToast(String message);
        void onClickToLogin();
        void onTextRegisterFound();
        void onRegisterSuccess(String username, String password);
    }
}

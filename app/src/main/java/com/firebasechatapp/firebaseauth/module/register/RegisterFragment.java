package com.firebasechatapp.firebaseauth.module.register;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebasechatapp.firebaseauth.R;
import com.firebasechatapp.firebaseauth.model.UserDetails;
import com.firebasechatapp.firebaseauth.model.UserRegisterDetails;
import com.firebasechatapp.firebaseauth.module.chat.ChatFragment;
import com.firebasechatapp.firebaseauth.module.login.LoginFragment;
import com.google.firebase.iid.FirebaseInstanceId;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class RegisterFragment extends Fragment implements RegisterConstructor.RegisterView, View.OnClickListener, View.OnFocusChangeListener {

    Unbinder unbinder;
    @BindView(R.id.tv_register_user_found)
    TextView tvUserFound;
    @BindView(R.id.edt_register_username)
    EditText edtUsername;
    @BindView(R.id.edt_register_password)
    EditText edtPassword;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.tv_click_login)
    TextView clickToLogin;
    @BindView(R.id.ln_register)
    LinearLayout lnRegister;

    private String username, password;
    private RegisterConstructor.RegisterSetPresenter registerPresenter;
    //////////////////////////////////////

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerPresenter = new RegisterPresenter(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*registerPresenter = new RegisterPresenter(this);*/
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.register_fragment, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lnRegister.setOnFocusChangeListener(this);
        clickToLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        edtUsername.setOnFocusChangeListener(this);
        edtPassword.setOnFocusChangeListener(this);
        lnRegister.setOnFocusChangeListener(this);

    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClickToLogin() {

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack();
        fragmentTransaction.commit();

    }

    @Override
    public void onTextRegisterFound() {
        tvUserFound.setVisibility(View.VISIBLE);

    }

    @Override
    public void onRegisterSuccess(String username, String password) {
        edtUsername.setText("");
        edtPassword.setText("");

        confirmDialogDemo(username, password);

    }

    private void confirmDialogDemo(final String username, final String password) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Go to Login");
        builder.setMessage("You are want to go to login now ?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                UserRegisterDetails.username = username;
                UserRegisterDetails.password = password;

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentManager.popBackStack();
                fragmentTransaction.commit();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentManager.popBackStack();
                fragmentTransaction.commit();
            }
        });

        builder.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:

                String deviceToken = FirebaseInstanceId.getInstance().getToken();
                String currentDeviceId = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                username = edtUsername.getText().toString().trim();
                password = edtPassword.getText().toString().trim();

                if (username.contentEquals("")) {
                    edtUsername.setError("Can not be blank");
                } else if (password.contentEquals("")) {
                    edtUsername.setError("Can not be blank");
                } else if (!username.matches("[A-Za-z0-9]+")) {
                    edtUsername.setError("Only alphabet or number allowed");
                /*} else if (username.length() < 4) {
                    edtUsername.setError("at least 5 characters long");
                } else if (password.length() < 4) {
                    edtUsername.setError("at least 5 characters long");*/
                } else {
                    registerPresenter.userRegister(username, password, deviceToken, currentDeviceId);
                }
                break;

            case R.id.tv_click_login:
                registerPresenter.clickLogin();
                break;
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        switch (view.getId()) {
            case R.id.edt_register_username:
                tvUserFound.setVisibility(View.GONE);
                break;

            case R.id.edt_register_password:
                tvUserFound.setVisibility(View.GONE);
                break;
            case R.id.ln_register:
                hideSoftKeyboard(view);
                break;
        }
    }

    private void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}

